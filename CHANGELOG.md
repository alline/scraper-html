# [3.0.0](https://gitlab.com/alline/scraper-html/compare/v2.0.0...v3.0.0) (2020-07-09)


### Bug Fixes

* update dependencies ([5d6bdfb](https://gitlab.com/alline/scraper-html/commit/5d6bdfb46371648e637daf0fb82b7dcea2617ac1))


### BREAKING CHANGES

* update @alline/core to v3

# [2.0.0](https://gitlab.com/alline/scraper-html/compare/v1.0.4...v2.0.0) (2020-07-02)


### Code Refactoring

* use RequestEpisodeScraper as base ([68ef058](https://gitlab.com/alline/scraper-html/commit/68ef058cf9b5af8cbe09c8ffd1c2d817d83d692f))


### BREAKING CHANGES

* hooks api changed

## [1.0.4](https://gitlab.com/alline/scraper-html/compare/v1.0.3...v1.0.4) (2020-06-29)


### Bug Fixes

* promise not resolve ([daccf59](https://gitlab.com/alline/scraper-html/commit/daccf594351dd6baa810976393af0286c8acc985))

## [1.0.3](https://gitlab.com/alline/scraper-html/compare/v1.0.2...v1.0.3) (2020-06-29)


### Bug Fixes

* encorde uri ([b9c08e9](https://gitlab.com/alline/scraper-html/commit/b9c08e94c082a160773019550ba8c31e7484947a))

## [1.0.2](https://gitlab.com/alline/scraper-html/compare/v1.0.1...v1.0.2) (2020-06-29)


### Bug Fixes

* add default option ([05b88b4](https://gitlab.com/alline/scraper-html/commit/05b88b410ebedf9119d6f0f159816f9169b85fc8))

## [1.0.1](https://gitlab.com/alline/scraper-html/compare/v1.0.0...v1.0.1) (2020-06-29)


### Bug Fixes

* update dependencies ([c4febee](https://gitlab.com/alline/scraper-html/commit/c4febeec5650850ac5001373bfee0675e4c02bb4))

# 1.0.0 (2020-05-20)


### Features

* initial commit ([7ec628c](https://gitlab.com/alline/scraper-html/commit/7ec628cff15efe44703f2a1de605abcce4c91569))
