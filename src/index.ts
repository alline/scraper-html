import { AxiosRequestConfig } from "axios";
import {
  RequestEpisodeScraper,
  RequestEpisodeScraperHook
} from "@alline/scraper-request";
import cheerio from "cheerio";

export interface HtmlEpisodeScraperOption {
  cheerio?: CheerioOptionsInterface;
  axios?: AxiosRequestConfig;
}

export interface HtmlEpisodeScraperHook
  extends RequestEpisodeScraperHook<CheerioStatic, string> {}

export class HtmlEpisodeScraper extends RequestEpisodeScraper<
  CheerioStatic,
  string
> {
  constructor(option: HtmlEpisodeScraperOption = {}) {
    const { axios, cheerio: cheerioOption } = option;
    super({
      axios,
      transform: async value => cheerio.load(value, cheerioOption),
      label: "HtmlEpisodeScraper"
    });
  }
}
